# Foundations of Python Programming

<details><summary>
<b>[1\. General Introduction](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/GeneralIntro/toctree.html)</b><br>
</summary>
-   [1.1. Introduction: The Way of the Program](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/GeneralIntro/intro-TheWayoftheProgram.html)<br>
-   [1.2. Algorithms](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/GeneralIntro/Algorithms.html)<br>
-   [1.3. The Python Programming Language](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/GeneralIntro/ThePythonProgrammingLanguage.html)<br>
-   [1.4. Special Ways to Execute Python in this Book](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/GeneralIntro/SpecialWaystoExecutePythoninthisBook.html)<br>
-   [1.5. More About Programs](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/GeneralIntro/MoreAboutPrograms.html)<br>
    -   [1.5.1. Preview of Control Structures](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/GeneralIntro/MoreAboutPrograms.html#preview-of-control-structures)<br>
-   [1.6. Formal and Natural Languages](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/GeneralIntro/FormalandNaturalLanguages.html)<br>
-   [1.7. A Typical First Program](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/GeneralIntro/ATypicalFirstProgram.html)<br>
-   [1.8. 👩💻 Predict Before You Run!](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/GeneralIntro/WPPredictBeforeYouRun.html)<br>
-   [1.9. 👩💻 To Understand a Program, Change It!](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/GeneralIntro/WPToUnderstandaProgramChangeIt.html)<br>
-   [1.10. Comments](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/GeneralIntro/Comments.html)<br>
-   [1.11. Glossary](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/GeneralIntro/Glossary.html)<br>
-   [1.12. Chapter Assessment](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/GeneralIntro/Exercises.html)<br>
    -   [1.12.1. Contributed Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/GeneralIntro/Exercises.html#contributed-exercises)
</details><details><summary>
<b>[2\. Variables, Statements, and Expressions](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/SimplePythonData/toctree.html)</b><br>  
</summary>
-   [2.1. Introduction](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/SimplePythonData/intro-VariablesExpressionsandStatements.html)<br>
    -   [2.1.1. Learning Goals](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/SimplePythonData/intro-VariablesExpressionsandStatements.html#learning-goals)<br>
    -   [2.1.2. Objectives](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/SimplePythonData/intro-VariablesExpressionsandStatements.html#objectives)<br>
-   [2.2. Values and Data Types](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/SimplePythonData/Values.html)<br>
-   [2.3. Operators and Operands](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/SimplePythonData/Operators.html)<br>
-   [2.4. Function Calls](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/SimplePythonData/FunctionCalls.html)<br>
    -   [2.4.1. Function calls as part of complex expressions](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/SimplePythonData/FunctionCalls.html#function-calls-as-part-of-complex-expressions)<br>
    -   [2.4.2. Functions are objects; parentheses invoke functions](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/SimplePythonData/FunctionCalls.html#functions-are-objects-parentheses-invoke-functions)<br>
-   [2.5. Data Types](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/SimplePythonData/DataTypes.html)<br>
-   [2.6. Type conversion functions](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/SimplePythonData/ConvertTypeFunctions.html)<br>
-   [2.7. Variables](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/SimplePythonData/Variables.html)<br>
-   [2.8. Variable Names and Keywords](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/SimplePythonData/VariableNamesandKeywords.html)<br>
-   [2.9. 👩💻 Choosing the Right Variable Name](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/SimplePythonData/WPChoosingtheRightVariableName.html)<br>
-   [2.10. Statements and Expressions](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/SimplePythonData/StatementsandExpressions.html)<br>
-   [2.11. Order of Operations](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/SimplePythonData/OrderofOperations.html)<br>
-   [2.12. Reassignment](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/SimplePythonData/Reassignment.html)<br>
    -   [2.12.1. Developing your mental model of How Python Evaluates](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/SimplePythonData/Reassignment.html#developing-your-mental-model-of-how-python-evaluates)<br>
-   [2.13. Updating Variables](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/SimplePythonData/UpdatingVariables.html)<br>
-   [2.14. 👩💻 Hard-Coding](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/SimplePythonData/HardCoding.html)<br>
-   [2.15. Input](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/SimplePythonData/Input.html)<br>
-   [2.16. Glossary](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/SimplePythonData/Glossary.html)<br>
-   [2.17. Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/SimplePythonData/Exercises.html)<br>
    -   [2.17.1. Contributed Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/SimplePythonData/Exercises.html#contributed-exercises)<br>
-   [2.18. Chapter Assessment](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/SimplePythonData/week1a2.html)
</details><details><summary>
<b>[3\. Debugging](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Debugging/toctree.html)</b><br>
</summary>
-   [3.1. Introduction to Debugging](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Debugging/intro-DebuggingGeneral.html)<br>
    -   [3.1.1. Learning Goals](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Debugging/intro-DebuggingGeneral.html#learning-goals)<br>
    -   [3.1.2. Objectives](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Debugging/intro-DebuggingGeneral.html#objectives)<br>
-   [3.2. 👩💻 Programming in the Real World](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Debugging/intro-HowtobeaSuccessfulProgrammer.html)<br>
-   [3.3. 👩💻 Debugging](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Debugging/intro-HowtobeaSuccessfulProgrammer.html#debugging)<br>
    -   [3.3.1. How to Avoid Debugging](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Debugging/intro-HowtobeaSuccessfulProgrammer.html#how-to-avoid-debugging)<br>
-   [3.4. 👩💻 Beginning tips for Debugging](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Debugging/BeginningtipsforDebugging.html)<br>
-   [3.5. Syntax errors](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Debugging/Syntaxerrors.html)<br>
-   [3.6. Runtime Errors](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Debugging/RuntimeErrors.html)<br>
-   [3.7. Semantic Errors](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Debugging/SemanticErrors.html)<br>
    -   [3.7.1. Test Cases](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Debugging/SemanticErrors.html#test-cases)<br>
-   [3.8. 👩💻 Know Your Error Messages](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Debugging/KnowyourerrorMessages.html)<br>
    -   [3.8.1. SyntaxError](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Debugging/KnowyourerrorMessages.html#syntaxerror)<br>
    -   [3.8.2. TypeError](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Debugging/KnowyourerrorMessages.html#typeerror)<br>
    -   [3.8.3. NameError](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Debugging/KnowyourerrorMessages.html#nameerror)<br>
    -   [3.8.4. ValueError](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Debugging/KnowyourerrorMessages.html#valueerror)<br>
-   [3.9. Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Debugging/Exercises.html)<br>
    -   [3.9.1. Contributed Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Debugging/Exercises.html#contributed-exercises)
</details><details><summary>
<b>[4\. Python Modules](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonModules/toctree.html)<br>
(`import`, `from`, `as`)</b><br>
</summary>
-   [4.1. Introduction to Python Modules](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonModules/intro-PythonModules.html)<br>
    -   [4.1.1. Learning Goals](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonModules/intro-PythonModules.html#learning-goals)<br>
    -   [4.1.2. Objectives](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonModules/intro-PythonModules.html#objectives)<br>
-   [4.2. Modules](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonModules/intro-ModulesandGettingHelp.html)<br>
    -   [4.2.1. Importing Modules](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonModules/intro-ModulesandGettingHelp.html#importing-modules)<br>
    -   [4.2.2. Syntax for Importing Modules and Functionality](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonModules/intro-ModulesandGettingHelp.html#syntax-for-importing-modules-and-functionality)<br>
-   [4.3. The `random` module](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonModules/Therandommodule.html)<br>
-   [4.4. Glossary](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonModules/Glossary.html)<br>
-   [4.5. Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonModules/Exercises.html)<br>
    -   [4.5.1. Contributed Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonModules/Exercises.html#contributed-exercises)
</details><details><summary>
<b>[5\. Python Turtle](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonTurtle/toctree.html)</b><br>
</summary>
-   [5.1. Hello Little Turtles!](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonTurtle/intro-HelloLittleTurtles.html)<br>
    -   [5.1.1. Learning Goals](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonTurtle/intro-HelloLittleTurtles.html#learning-goals)<br>
    -   [5.1.2. Objectives](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonTurtle/intro-HelloLittleTurtles.html#objectives)<br>
-   [5.2. Our First Turtle Program](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonTurtle/OurFirstTurtleProgram.html)<br>
-   [5.3. Instances: A Herd of Turtles](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonTurtle/InstancesAHerdofTurtles.html)<br>
-   [5.4. Object Oriented Concepts](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonTurtle/ObjectInstances.html)<br>
    -   [5.4.1. User-defined Classes](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonTurtle/ObjectInstances.html#user-defined-classes)<br>
    -   [5.4.2. Instances](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonTurtle/ObjectInstances.html#instances)<br>
    -   [5.4.3. Attributes](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonTurtle/ObjectInstances.html#attributes)<br>
    -   [5.4.4. Methods](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonTurtle/ObjectInstances.html#methods)<br>
-   [5.5. Repetition with a For Loop](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonTurtle/RepetitionwithaForLoop.html)<br>
-   [5.6. A Few More `turtle` Methods and Observations](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonTurtle/AFewMoreturtleMethodsandObservations.html)<br>
-   [5.7. Summary of Turtle Methods](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonTurtle/SummaryOfTurtleMethods.html)<br>
-   [5.8. 👩💻 Incremental Programming](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonTurtle/WPIncrementalProgramming.html)<br>
-   [5.9. 👩💻 Common `turtle` Errors](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonTurtle/WPCommonTurtleErrors.html)<br>
-   [5.10. Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonTurtle/Exercises.html)<br>
    -   [5.10.1. Contributed Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonTurtle/Exercises.html#contributed-exercises)<br>
-   [5.11. Chapter Assessment - Turtle and Object Mechanics](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonTurtle/week1a3.html)<br>
-   [5.12. Chapter Assessment - Drawing with Turtle](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/PythonTurtle/week1a3.html#chapter-assessment-drawing-with-turtle)
</details><details><summary>
<b>[6\. Sequences](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sequences/toctree.html)<br>
(`list`, `str`)</b><br>
</summary>
-   [6.1. Introduction: Sequences](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sequences/intro-Sequences.html)<br>
    -   [6.1.1. Learning Goals](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sequences/intro-Sequences.html#learning-goals)<br>
    -   [6.1.2. Objectives](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sequences/intro-Sequences.html#objectives)<br>
-   [6.2. Strings and Lists](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sequences/StringsandLists.html)<br>
    -   [6.2.1. Strings](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sequences/StringsandLists.html#strings)<br>
    -   [6.2.2. Lists](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sequences/StringsandLists.html#lists)<br>
    -   [6.2.3. Tuples](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sequences/StringsandLists.html#tuples)<br>
-   [6.3. Index Operator: Working with the Characters of a String](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sequences/IndexOperatorWorkingwiththeCharactersofaString.html)<br>
    -   [6.3.1. Index Operator: Accessing Elements of a List or Tuple](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sequences/IndexOperatorWorkingwiththeCharactersofaString.html#index-operator-accessing-elements-of-a-list-or-tuple)<br>
-   [6.4. Disambiguating \[\]: creation vs indexing](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sequences/DisabmiguatingSquareBrackets.html)<br>
-   [6.5. Length](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sequences/Length.html)<br>
-   [6.6. The Slice Operator](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sequences/TheSliceOperator.html)<br>
    -   [6.6.1. List Slices](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sequences/TheSliceOperator.html#list-slices)<br>
    -   [6.6.2. Tuple Slices](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sequences/TheSliceOperator.html#tuple-slices)<br>
-   [6.7. Concatenation and Repetition](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sequences/ConcatenationandRepetition.html)<br>
-   [6.8. Count and Index](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sequences/CountandIndex.html)<br>
    -   [6.8.1. Count](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sequences/CountandIndex.html#count)<br>
    -   [6.8.2. Index](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sequences/CountandIndex.html#index)<br>
-   [6.9. Splitting and Joining Strings](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sequences/SplitandJoin.html)<br>
-   [6.10. Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sequences/Exercises.html)<br>
    -   [6.10.1. Contributed Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sequences/Exercises.html#contributed-exercises)<br>
-   [6.11. Chapter Assessment](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sequences/week2a1.html)
</details><details><summary>
<b>[7\. Iteration](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Iteration/toctree.html)<br>
(`for`, `str`, `range`, `list`, `index`, `[]`, _accumulator pattern_, _nested for loops_)</b><br>
</summary>
-   [7.1. Introduction: Iteration](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Iteration/intro-Iteration.html)<br>
-   [7.2. The </b>for</b> Loop](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Iteration/TheforLoop.html)<br>
-   [7.3. Flow of Execution of the for Loop](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Iteration/FlowofExecutionoftheforLoop.html)<br>
-   [7.4. Strings and `for` loops](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Iteration/Stringsandforloops.html)<br>
-   [7.5. Lists and `for` loops](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Iteration/Listsandforloops.html)<br>
    -   [7.5.1. Using the range Function to Generate a Sequence to Iterate Over](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Iteration/Listsandforloops.html#using-the-range-function-to-generate-a-sequence-to-iterate-over)<br>
    -   [7.5.2. Iteration Simplifies our Turtle Program](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Iteration/Listsandforloops.html#iteration-simplifies-our-turtle-program)<br>
-   [7.6. The Accumulator Pattern](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Iteration/TheAccumulatorPattern.html)<br>
-   [7.7. Traversal and the `for` Loop: By Index](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Iteration/TraversalandtheforLoopByIndex.html)<br>
-   [7.8. Nested Iteration: Image Processing](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Iteration/NestedIterationImageProcessing.html)<br>
    -   [7.8.1. The RGB Color Model](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Iteration/NestedIterationImageProcessing.html#the-rgb-color-model)<br>
    -   [7.8.2. Image Objects](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Iteration/NestedIterationImageProcessing.html#image-objects)<br>
    -   [7.8.3. Image Processing and Nested Iteration](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Iteration/NestedIterationImageProcessing.html#image-processing-and-nested-iteration)<br>
-   [7.9. 👩💻 Printing Intermediate Results](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Iteration/WPPrintingIntermediateResults.html)<br>
-   [7.10. 👩💻 Naming Variables in For Loops](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Iteration/WPNamingVariablesinForLoops.html)<br>
-   [7.11. The Gory Details: Iterables](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Iteration/GeneralizedSequences.html)<br>
-   [7.12. 👩💻 Keeping Track of Your Iterator Variable and Your Iterable](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Iteration/WPKeepingTrackofYourIteratorVariableYourIterable.html)<br>
-   [7.13. Glossary](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Iteration/Glossary.html)<br>
-   [7.14. Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Iteration/Exercises.html)<br>
    -   [7.14.1. Contributed Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Iteration/Exercises.html#contributed-exercises)<br>
-   [7.15. Chapter Assessment](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Iteration/week2a2.html)
</details><details><summary>
<b>[8\. Conditionals](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Conditionals/toctree.html)<br>
(`if`, `else`, `elif`, `bool`, `>`, `==`, `<`, `not`, `and`, `or`, `is`, `!=`, `max`, `min`, `continue`, `break`, _unary selection_, _nested conditionals_)</b><br>
</summary>
-   [8.1. Intro: What we can do with Turtles and Conditionals](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Conditionals/intro-TurtlesandConditionals.html)<br>
    -   [8.1.1. Learning Goals](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Conditionals/intro-TurtlesandConditionals.html#learning-goals)<br>
    -   [8.1.2. Objectives](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Conditionals/intro-TurtlesandConditionals.html#objectives)<br>
-   [8.2. Boolean Values and Boolean Expressions](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Conditionals/BooleanValuesandBooleanExpressions.html)<br>
-   [8.3. Logical operators](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Conditionals/Logicaloperators.html)<br>
    -   [8.3.1. Smart Evaluation](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Conditionals/Logicaloperators.html#smart-evaluation)<br>
-   [8.4. The `in` and `not in` operators](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Conditionals/Theinandnotinoperators.html)<br>
-   [8.5. Precedence of Operators](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Conditionals/PrecedenceofOperators.html)<br>
-   [8.6. Conditional Execution: Binary Selection](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Conditionals/ConditionalExecutionBinarySelection.html)<br>
-   [8.7. Omitting the `else` Clause: Unary Selection](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Conditionals/OmittingtheelseClauseUnarySelection.html)<br>
-   [8.8. Nested conditionals](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Conditionals/Nestedconditionals.html)<br>
-   [8.9. Chained conditionals](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Conditionals/Chainedconditionals.html)<br>
-   [8.10. The Accumulator Pattern with Conditionals](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Conditionals/TheAccumulatorPatternwithConditionals.html)<br>
    -   [8.10.1. Accumulating the Max Value](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Conditionals/TheAccumulatorPatternwithConditionals.html#accumulating-the-max-value)<br>
-   [8.11. 👩💻 Setting Up Conditionals](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Conditionals/WPSettingUpConditionals.html)<br>
    -   [8.11.1. Choosing your type of Conditional](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Conditionals/WPSettingUpConditionals.html#choosing-your-type-of-conditional)<br>
-   [8.12. Glossary](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Conditionals/Glossary.html)<br>
-   [8.13. Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Conditionals/Exercises.html)<br>
    -   [8.13.1. Contributed Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Conditionals/Exercises.html#contributed-exercises)<br>
-   [8.14. Chapter Assessment](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Conditionals/week3a1.html)
</details><details><summary>
<b>[9\. Transforming Sequences](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/toctree.html)<br>
(`list`, `str`, `tuple`, `[:]`, _object references_, _mutability_, _aliasing_, _cloning_ with _slice_, _accumulator pattern_)</b><br>  
</summary>
-   [9.1. Introduction: Transforming Sequences](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/intro-SequenceMutation.html)<br>
    -   [9.1.1. Learning Goals](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/intro-SequenceMutation.html#learning-goals)<br>
    -   [9.1.2. Objectives](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/intro-SequenceMutation.html#objectives)<br>
-   [9.2. Mutability](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/Mutability.html)<br>
    -   [9.2.1. Lists are Mutable](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/Mutability.html#lists-are-mutable)<br>
    -   [9.2.2. Strings are Immutable](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/Mutability.html#strings-are-immutable)<br>
    -   [9.2.3. Tuples are Immutable](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/Mutability.html#tuples-are-immutable)<br>
-   [9.3. List Element Deletion](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/ListDeletion.html)<br>
-   [9.4. Objects and References](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/ObjectsandReferences.html)<br>
-   [9.5. Aliasing](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/Aliasing.html)<br>
-   [9.6. Cloning Lists](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/CloningLists.html)<br>
-   [9.7. Mutating Methods](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/MutatingMethods.html)<br>
    -   [9.7.1. List Methods](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/MutatingMethods.html#list-methods)<br>
-   [9.8. Append versus Concatenate](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/AppendversusConcatenate.html)<br>
-   [9.9. Non-mutating Methods on Strings](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/NonmutatingMethodsonStrings.html)<br>
    -   [9.9.1. String Format Method](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/NonmutatingMethodsonStrings.html#string-format-method)<br>
-   [9.10. The Accumulator Pattern with Lists](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/TheAccumulatorPatternwithLists.html)<br>
-   [9.11. The Accumulator Pattern with Strings](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/TheAccumulatorPatternwithStrings.html)<br>
-   [9.12. 👩💻 Accumulator Pattern Strategies](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/WPAccumulatorPatternStrategies.html)<br>
    -   [9.12.1. When to Use it](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/WPAccumulatorPatternStrategies.html#when-to-use-it)<br>
    -   [9.12.2. Before Writing it](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/WPAccumulatorPatternStrategies.html#before-writing-it)<br>
    -   [9.12.3. Choosing Good Accumulator and Iterator Variable Names](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/WPAccumulatorPatternStrategies.html#choosing-good-accumulator-and-iterator-variable-names)<br>
-   [9.13. 👩💻 Don’t Mutate A List That You Are Iterating Through](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/WPDontMutateAListYouIterateThrough.html)<br>
-   [9.14. Summary](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/Glossary.html)<br>
-   [9.15. Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/Exercises.html)<br>
    -   [9.15.1. Contributed Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/Exercises.html#contributed-exercises)<br>
-   [9.16. Chapter Assessment - List Methods](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/week4a1.html)<br>
    -   [9.16.1. Chapter Assessment - Aliases and References](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/week4a1.html#chapter-assessment-aliases-and-references)<br>
    -   [9.16.2. Chapter Assessment - Split and Join](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/week4a1.html#chapter-assessment-split-and-join)<br>
    -   [9.16.3. Chapter Assessment - For Loop Mechanics](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/week4a1.html#chapter-assessment-for-loop-mechanics)<br>
    -   [9.16.4. Chapter Assessment - Accumulator Pattern](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/week4a1.html#chapter-assessment-accumulator-pattern)<br>
    -   [9.16.5. Chapter Assessment - Problem Solving](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TransformingSequences/week4a1.html#chapter-assessment-problem-solving)
</details><details><summary>
<b>[10\. Files](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Files/toctree.html)<br>
(`open`, `read`, `write`, `with`, _iterate over file lines_, _context variables_)</b><br>
</summary>
-   [10.1. Introduction: Working with Data Files](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Files/intro-WorkingwithDataFiles.html)<br>
    -   [10.1.1. Learning Goals](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Files/intro-WorkingwithDataFiles.html#learning-goals)<br>
    -   [10.1.2. Objectives](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Files/intro-WorkingwithDataFiles.html#objectives)<br>
-   [10.2. Reading a File](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Files/ReadingaFile.html)<br>
-   [10.3. Alternative File Reading Methods](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Files/AlternativeFileReadingMethods.html)<br>
-   [10.4. Iterating over lines in a file](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Files/Iteratingoverlinesinafile.html)<br>
-   [10.5. Finding a File in your Filesystem](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Files/FindingaFileonyourDisk.html)<br>
-   [10.6. Using `with` for Files](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Files/With.html)<br>
-   [10.7. Recipe for Reading and Processing a File](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Files/FilesRecipe.html)<br>
-   [10.8. Writing Text Files](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Files/WritingTextFiles.html)<br>
-   [10.9. CSV Format](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Files/CSVFormat.html)<br>
-   [10.10. Reading in data from a CSV File](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Files/ReadingCSVFiles.html)<br>
-   [10.11. Writing data to a CSV File](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Files/WritingCSVFiles.html)<br>
-   [10.12. 👩💻 Tips on Handling Files](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Files/WPTipsHandlingFiles.html)<br>
-   [10.13. Glossary](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Files/Glossary.html)<br>
-   [10.14. Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Files/Exercises.html)<br>
    -   [10.14.1. Contributed Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Files/Exercises.html#contributed-exercises)<br>
-   [10.15. Chapter Assessment](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Files/ChapterAssessment.html)</b>
</details><details><summary>
<b>[11\. Dictionaries](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Dictionaries/toctree.html)<br>
(`dict`, `.keys()`, `.values()`, `.items()`, `[]`, _copying dictionaries_, _accumulation pattern_, _hash map_ or _hash table_)
</summary>
-   [11.1. Introduction: Dictionaries](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Dictionaries/intro-DictionaryGoals.html)<br>
    -   [11.1.1. Learning Goals](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Dictionaries/intro-DictionaryGoals.html#learning-goals)<br>
    -   [11.1.2. Objectives](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Dictionaries/intro-DictionaryGoals.html#objectives)<br>
-   [11.2. Getting Started with Dictionaries](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Dictionaries/intro-Dictionaries.html)<br>
-   [11.3. Dictionary operations](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Dictionaries/Dictionaryoperations.html)<br>
-   [11.4. Dictionary methods](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Dictionaries/Dictionarymethods.html)<br>
    -   [11.4.1. Iterating over Dictionaries](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Dictionaries/Dictionarymethods.html#iterating-over-dictionaries)<br>
    -   [11.4.2. Safely Retrieving Values](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Dictionaries/Dictionarymethods.html#safely-retrieving-values)<br>
-   [11.5. Aliasing and copying](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Dictionaries/Aliasingandcopying.html)<br>
-   [11.6. Introduction: Accumulating Multiple Results In a Dictionary](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Dictionaries/intro-AccumulatingMultipleResultsInaDictionary.html)<br>
-   [11.7. Accumulating Results From a Dictionary](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Dictionaries/AccumulatingResultsFromaDictionary.html)<br>
-   [11.8. Accumulating the Best Key](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Dictionaries/AccumulatingtheBestKey.html)<br>
-   [11.9. 👩💻 When to use a dictionary](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Dictionaries/WPChoosingDictionaries.html)<br>
-   [11.10. Glossary](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Dictionaries/Glossary.html)<br>
-   [11.11. Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Dictionaries/Exercises.html)<br>
    -   [11.11.1. Contributed Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Dictionaries/Exercises.html#contributed-exercises)<br>
-   [11.12. Chapter Assessment](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Dictionaries/ChapterAssessment.html)<br>
    -   [11.12.1. Assessment - Dictionaries](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Dictionaries/ChapterAssessment.html#assessment-dictionaries)<br>
    -   [11.12.2. Assessment - Dictionary Accumulation](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Dictionaries/ChapterAssessment.html#assessment-dictionary-accumulation)</b>
</details><details><summary>
<b>[12\. Functions](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Functions/toctree.html)<br>
(`def`, `return`, `()`, `global`, _function calls_, _function definitions_, _named keyword arguments_, _positional arguments_)</b><br>s
</summary>
-   [12.1. Introduction to Functions](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Functions/intro-Functions.html)<br>
    -   [12.1.1. Topics](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Functions/intro-Functions.html#topics)<br>
    -   [12.1.2. Learning Objectives](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Functions/intro-Functions.html#learning-objectives)<br>
-   [12.2. Function Definition](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Functions/FunctionDefinitions.html)<br>
-   [12.3. Function Invocation](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Functions/FunctionInvocation.html)<br>
-   [12.4. Function Parameters](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Functions/FunctionParameters.html)<br>
-   [12.5. Returning a value from a function](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Functions/Returningavaluefromafunction.html)<br>
-   [12.6. 👩💻 Decoding a Function](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Functions/DecodingaFunction.html)<br>
-   [12.7. Type Annotations](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Functions/TypeAnnotations.html)<br>
-   [12.8. A function that accumulates](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Functions/Afunctionthataccumulates.html)<br>
-   [12.9. Variables and parameters are local](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Functions/Variablesandparametersarelocal.html)<br>
-   [12.10. Global Variables](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Functions/GlobalVariables.html)<br>
-   [12.11. Functions can call other functions (Composition)](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Functions/Functionscancallotherfunctions.html)<br>
-   [12.12. Flow of Execution Summary](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Functions/FlowofExecutionSummary.html)<br>
-   [12.13. 👩💻 Print vs. return](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Functions/Printvsreturn.html)<br>
-   [12.14. Passing Mutable Objects](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Functions/PassingMutableObjects.html)<br>
-   [12.15. Side Effects](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Functions/SideEffects.html)<br>
-   [12.16. Glossary](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Functions/Glossary.html)<br>
-   [12.17. Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Functions/Exercises.html)<br>
    -   [12.17.1. Contributed Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Functions/Exercises.html#contributed-exercises)<br>
-   [12.18. Chapter Assessment](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Functions/ChapterAssessment.html)
</details><details><summary>
<b>[13\. Tuple Packing and Unpacking](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Tuples/toctree.html)<br>
(`tuple`, `enumerate`, _enumerate sequence items_, _packing_, _unpacking_, _swapping values_, _returning tuples_)</b><br>
</summary>
-   [13.1. Introduction](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Tuples/TuplePacking-Intro.html)<br>
    -   [13.1.1. Learning Objectives](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Tuples/TuplePacking-Intro.html#learning-objectives)<br>
-   [13.2. Tuple Packing](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Tuples/TuplePacking.html)<br>
-   [13.3. Tuple Assignment with Unpacking](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Tuples/TupleAssignmentwithunpacking.html)<br>
    -   [13.3.1. Swapping Values between Variables](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Tuples/TupleAssignmentwithunpacking.html#swapping-values-between-variables)<br>
    -   [13.3.2. Unpacking Into Iterator Variables](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Tuples/TupleAssignmentwithunpacking.html#unpacking-into-iterator-variables)<br>
    -   [13.3.3. The Pythonic Way to Enumerate Items in a Sequence](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Tuples/TupleAssignmentwithunpacking.html#the-pythonic-way-to-enumerate-items-in-a-sequence)<br>
-   [13.4. Tuples as Return Values](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Tuples/TuplesasReturnValues.html)<br>
-   [13.5. Unpacking Tuples as Arguments to Function Calls](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Tuples/UnpackingArgumentsToFunctions.html)<br>
-   [13.6. Glossary](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Tuples/Glossary.html)<br>
-   [13.7. Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Tuples/Exercises.html)<br>
    -   [13.7.1. Contributed Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Tuples/Exercises.html#contributed-exercises)<br>
-   [13.8. Chapter Assessment](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Tuples/ChapterAssessment.html)
</details><details><summary>
<b>[14\. More About Iteration](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/MoreAboutIteration/toctree.html)<br>
(`while`, `break`, `continue`, _listener loop pattern_, _sentinel values_, _random walk_, _halting_, _infinite loop_)</b><br>
</summary>
-   [14.1. Introduction](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/MoreAboutIteration/intro-indefiniteiteration.html)<br>
    -   [14.1.1. Learning Goals](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/MoreAboutIteration/intro-indefiniteiteration.html#learning-goals)<br>
    -   [14.1.2. Objectives](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/MoreAboutIteration/intro-indefiniteiteration.html#objectives)<br>
-   [14.2. The `while` Statement](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/MoreAboutIteration/ThewhileStatement.html)<br>
-   [14.3. The Listener Loop](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/MoreAboutIteration/listenerLoop.html)<br>
    -   [14.3.1. Other uses of `while`](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/MoreAboutIteration/listenerLoop.html#other-uses-of-while)<br>
        -   [14.3.1.1. Sentinel Values](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/MoreAboutIteration/listenerLoop.html#sentinel-values)<br>
        -   [14.3.1.2. Validating Input](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/MoreAboutIteration/listenerLoop.html#validating-input)<br>
-   [14.4. Randomly Walking Turtles](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/MoreAboutIteration/RandomlyWalkingTurtles.html)<br>
-   [14.5. Break and Continue](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/MoreAboutIteration/BreakandContinue.html)<br>
-   [14.6. 👩💻 Infinite Loops](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/MoreAboutIteration/WPInfiniteLoops.html)<br>
-   [14.7. Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/MoreAboutIteration/Exercises.html)<br>
    -   [14.7.1. Contributed Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/MoreAboutIteration/Exercises.html#contributed-exercises)<br>
-   [14.8. Chapter Assessment](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/MoreAboutIteration/ChapterAssessment.html)
</details><details><summary>
<b>[15\. Advanced Functions](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/AdvancedFunctions/toctree.html)<br>
(`kwargs`, `lambda`, _keyword paramaters_ (keyword arguments), _anonymous functions_, _python style_ (PEP8), _methods_)</b><br>
</summary>
-   [15.1. Introduction: Optional Parameters](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/AdvancedFunctions/OptionalParameters.html)<br>
-   [15.2. Keyword Parameters](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/AdvancedFunctions/KeywordParameters.html)<br>
    -   [15.2.1. Keyword Parameters with .format](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/AdvancedFunctions/KeywordParameters.html#keyword-parameters-with-format)<br>
-   [15.3. Anonymous functions with lambda expressions](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/AdvancedFunctions/Anonymousfunctionswithlambdaexpressions.html)<br>
-   [15.4. 👩💻 Programming With Style](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/AdvancedFunctions/ProgrammingWithStyle.html)<br>
-   [15.5. Method Invocations](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/AdvancedFunctions/MethodInvocations.html)<br>
-   [15.6. Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/AdvancedFunctions/Exercises.html)<br>
    -   [15.6.1. Contributed Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/AdvancedFunctions/Exercises.html#contributed-exercises)<br>
-   [15.7. Chapter Assessment](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/AdvancedFunctions/ChapterAssessment.html)
</details><details><summary>
<b>[16\. Sorting](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sorting/toctree.html)<br>
(`sort`, `sorted`, `reversed`, `dict`, _sorting a dict_)</b><br>  
</summary>
-   [16.1. Introduction: Sorting with Sort and Sorted](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sorting/intro-SortingwithSortandSorted.html)<br>
-   [16.2. Optional reverse parameter](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sorting/Optionalreverseparameter.html)<br>
-   [16.3. Optional key parameter](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sorting/Optionalkeyparameter.html)<br>
-   [16.4. Sorting a Dictionary](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sorting/SortingaDictionary.html)<br>
-   [16.5. Breaking Ties: Second Sorting](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sorting/SecondarySortOrder.html)<br>
-   [16.6. 👩💻 When to use a Lambda Expression](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sorting/WPWhenToUseLambdaVsFunction.html)<br>
-   [16.7. Glossary](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sorting/Glossary.html)<br>
-   [16.8. Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sorting/Exercises.html)<br>
    -   [16.8.1. Contributed Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sorting/Exercises.html#contributed-exercises)<br>
-   [16.9. Chapter Assessment](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Sorting/ChapterAssessment.html)
</details><details><summary>
<b>[17\. Nested Data and Nested Iteration](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/NestedData/toctree.html)<br>
(`json`, `itertools`, `_dict_`, `list`, _nested data_, _data structures_, _nested dictionaries_)</b><br>  
</summary>
-   [17.1. Introduction: Nested Data and Nested Iteration](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/NestedData/ListswithComplexItems.html)<br>
    -   [17.1.1. Lists with Complex Items](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/NestedData/ListswithComplexItems.html#lists-with-complex-items)<br>
-   [17.2. Nested Dictionaries](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/NestedData/NestedDictionaries.html)<br>
-   [17.3. Processing JSON results](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/NestedData/jsonlib.html)<br>
-   [17.4. Nested Iteration](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/NestedData/NestedIteration.html)<br>
-   [17.5. 👩💻 Structuring Nested Data](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/NestedData/WPStructuringNestedData.html)<br>
-   [17.6. Deep and Shallow Copies](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/NestedData/DeepandShallowCopies.html)<br>
-   [17.7. 👩💻 Extracting from Nested Data](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/NestedData/WPExtractFromNestedData.html)<br>
    -   [17.7.1. Understand](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/NestedData/WPExtractFromNestedData.html#understand)<br>
    -   [17.7.2. Extract](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/NestedData/WPExtractFromNestedData.html#extract)<br>
    -   [17.7.3. Repeat](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/NestedData/WPExtractFromNestedData.html#repeat)<br>
        -   [17.7.3.1. Level 2](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/NestedData/WPExtractFromNestedData.html#level-2)<br>
        -   [17.7.3.2. Level 3](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/NestedData/WPExtractFromNestedData.html#level-3)<br>
        -   [17.7.3.3. Level 4](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/NestedData/WPExtractFromNestedData.html#level-4)<br>
        -   [17.7.3.4. Reflections](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/NestedData/WPExtractFromNestedData.html#reflections)<br>
-   [17.8. Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/NestedData/Exercises.html)<br>
    -   [17.8.1. Contributed Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/NestedData/Exercises.html#contributed-exercises)<br>
-   [17.9. Chapter Assessment](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/NestedData/ChapterAssessment.html)
</details><details><summary>
<b>[18\. Test Cases](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TestCases/toctree.html)<br>
(_unit tests_, _testing loops_, _testing logic_, _testing functions_, _side effects_, _optional function arguments_)</b><br>  
</summary>
-   [18.1. Introduction: Test Cases](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TestCases/intro-TestCases.html)<br>
-   [18.2. Checking Assumptions About Data Types](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TestCases/TestingTypes.html)<br>
-   [18.3. Checking Other Assumptions](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TestCases/TestingTypes.html#checking-other-assumptions)<br>
-   [18.4. Testing Conditionals](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TestCases/TestingConditionals.html)<br>
-   [18.5. Testing Loops](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TestCases/TestingLoops.html)<br>
-   [18.6. Writing Test Cases for Functions](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TestCases/Testingfunctions.html)<br>
    -   [18.6.1. Return Value Tests](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TestCases/Testingfunctions.html#return-value-tests)<br>
    -   [18.6.2. Side Effect Tests](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TestCases/Testingfunctions.html#side-effect-tests)<br>
-   [18.7. Testing Optional Parameters](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TestCases/TestingOptionalParameters.html)<br>
-   [18.8. 👩💻 Program Development](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TestCases/WPProgramDevelopment.html)<br>
-   [18.9. Glossary](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TestCases/Glossary.html)<br>
-   [18.10. Chapter Assessment](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TestCases/ChapterAssessment.html)<br>
-   [18.11. Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TestCases/Exercises.html)<br>
    -   [18.11.1. Contributed Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/TestCases/Exercises.html#contributed-exercises)
</details><details><summary>
<b>[19\. Exceptions](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Exceptions/toctree.html)<br>
(`raise`, `try`, `except`, `finaly`, `Exception`, _standard exceptions_ _traceback_)</b><br>  
</summary>
-   [19.1. What is an exception?](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Exceptions/intro-exceptions.html)<br>
-   [19.2. Exception Handling Flow-of-control](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Exceptions/intro-exceptions.html#exception-handling-flow-of-control)<br>
    -   [19.2.1. Raising and Catching Errors](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Exceptions/intro-exceptions.html#raising-and-catching-errors)<br>
-   [19.3. 👩💻 When to use try/except](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Exceptions/using-exceptions.html)<br>
-   [19.4. Standard Exceptions](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Exceptions/standard-exceptions.html)<br>
-   [19.5. Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Exceptions/Exercises.html)<br>
    -   [19.5.1. Contributed Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Exceptions/Exercises.html#contributed-exercises)<br>
-   [19.6. Chapter Assessment](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Exceptions/ChapterAssessment.html)
</details><details><summary>
<b>[20\. Defining your own Classes](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Classes/toctree.html)<br>
(`class`, `self`, `__init__`, `__str__`, `str`, `repr`, `_sorting objects_`, _instance methods_, _class methods_, _class attributes_, _object oriented programming_)</b><br>  
</summary>
-   [20.1. Introduction: Classes and Objects - the Basics](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Classes/intro-ClassesandObjectstheBasics.html)<br>
    -   [20.1.1. Object-oriented programming](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Classes/intro-ClassesandObjectstheBasics.html#object-oriented-programming)<br>
-   [20.2. Objects Revisited](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Classes/ObjectsRevisited.html)<br>
-   [20.3. User Defined Classes](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Classes/UserDefinedClasses.html)<br>
-   [20.4. Adding Parameters to the Constructor](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Classes/ImprovingourConstructor.html)<br>
-   [20.5. Adding Other Methods to a Class](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Classes/AddingOtherMethodstoourClass.html)<br>
-   [20.6. Objects as Arguments and Parameters](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Classes/ObjectsasArgumentsandParameters.html)<br>
-   [20.7. Converting an Object to a String](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Classes/ConvertinganObjecttoaString.html)<br>
-   [20.8. Instances as Return Values](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Classes/InstancesasReturnValues.html)<br>
-   [20.9. Sorting Lists of Instances](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Classes/sorting_instances.html)<br>
-   [20.10. Class Variables and Instance Variables](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Classes/ClassVariablesInstanceVariables.html)<br>
-   [20.11. Thinking About Classes and Instances](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Classes/ThinkingAboutClasses.html)<br>
-   [20.12. Testing classes](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Classes/TestingClasses.html)<br>
-   [20.13. A Tamagotchi Game](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Classes/Tamagotchi.html)<br>
-   [20.14. Glossary](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Classes/Glossary.html)<br>
-   [20.15. Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Classes/Exercises.html)<br>
    -   [20.15.1. Contributed Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Classes/Exercises.html#contributed-exercises)<br>
-   [20.16. Chapter Assessment](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Classes/ChapterAssessment.html)
</details><details><summary>
<b>[21\. Building Programs](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/BuildingPrograms/toctree.html)</b><br>  
</summary>
-   [21.1. Building A Program: A Strategy](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/BuildingPrograms/TheStrategy.html)<br>
    -   [21.1.1. Sketch an Outline](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/BuildingPrograms/TheStrategy.html#sketch-an-outline)<br>
    -   [21.1.2. Code One Section at a Time](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/BuildingPrograms/TheStrategy.html#code-one-section-at-a-time)<br>
    -   [21.1.3. Clean Up](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/BuildingPrograms/TheStrategy.html#clean-up)<br>
-   [21.2. 👩💻 Sketch an Outline](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/BuildingPrograms/WPSketchanOutline.html)<br>
-   [21.3. 👩💻 Code one section at a time](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/BuildingPrograms/WPCodeSectionataTime.html)<br>
-   [21.4. 👩💻 Clean Up](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/BuildingPrograms/WPCleanCode.html)<br>
-   [21.5. Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/BuildingPrograms/Exercises.html)<br>
    -   [21.5.1. Contributed Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/BuildingPrograms/Exercises.html#contributed-exercises)
</details><details><summary>
<b>[22\. Inheritance](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Inheritance/toctree.html)</b><br>
</summary>
-   [22.1. Introduction: Class Inheritance](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Inheritance/intro.html)<br>
-   [22.2. Inheriting Variables and Methods](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Inheritance/inheritVarsAndMethods.html)<br>
    -   [22.2.1. Mechanics of Defining a Subclass](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Inheritance/inheritVarsAndMethods.html#mechanics-of-defining-a-subclass)<br>
    -   [22.2.2. How the interpreter looks up attributes](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Inheritance/inheritVarsAndMethods.html#how-the-interpreter-looks-up-attributes)<br>
-   [22.3. Overriding Methods](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Inheritance/OverrideMethods.html)<br>
-   [22.4. Invoking the Parent Class’s Method](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Inheritance/InvokingSuperMethods.html)<br>
-   [22.5. Tamagotchi Revisited](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Inheritance/TamagotchiRevisited.html)<br>
-   [22.6. Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Inheritance/Exercises.html)<br>
    -   [22.6.1. Contributed Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Inheritance/Exercises.html#contributed-exercises)<br>
-   [22.7. Chapter Assessment](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Inheritance/ChapterAssessment.html)<br>
-   [22.8. Project - Wheel of Python](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Inheritance/chapterProject.html)
</details><details><summary>
<b>[23\. More Accumulation: Map, Filter, List Comprehension, & Zip](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/AdvancedAccumulation/toctree.html)<br>
(`map`, `filter`, `zip`, _list comprehensions_)</b><br>  
</summary>
-   [23.1. Introduction: Map, Filter, List Comprehensions, & Zip](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/AdvancedAccumulation/intro.html)<br>
-   [23.2. Map](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/AdvancedAccumulation/map.html)<br>
-   [23.3. Filter](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/AdvancedAccumulation/filter.html)<br>
-   [23.4. List Comprehensions](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/AdvancedAccumulation/listcomp.html)<br>
-   [23.5. Zip](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/AdvancedAccumulation/zip.html)<br>
-   [23.6. Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/AdvancedAccumulation/Exercises.html)<br>
    -   [23.6.1. Contributed Exercises](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/AdvancedAccumulation/Exercises.html#contributed-exercises)<br>
-   [23.7. Chapter Assessment](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/AdvancedAccumulation/ChapterAssessment.html)
</details>