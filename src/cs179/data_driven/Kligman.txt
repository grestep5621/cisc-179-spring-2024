starttownCenterChoices
You are in the town center.


1. Enter the bar
2. Enter the shop
3. Enter the palace
4. Walk to the temple

endtownCenterChoices

startbarChoices
You are in the bar. You spot the burly man in the corner!


1. Attack the burly man
2. Buy the burly man a drink
3. Buy yourself a drink
4. Back to town center

endbarChoices

startshopChoices
You are in the shop.


1. Buy 'Pint Of Intoxicant'
2. Buy gun
3. Back to town center

endshopChoices

starttempleChoices
You are standing in front of the temple. There is a sprawling mass of
vegetation that covers the temple walls and the wet earth.


1. Enter the temple
2. Ask monk about the plant
3. Pick some of the plant
4. Back to town center

endtempleChoices

startinTempleChoices
As you enter the temple, all eyes fall on you.

Head Monk: "We welcome you, stranger. If you wish to join our school of
thought, you must journey to the holy mountain alone. It is quite the
endeavor, but it is the only way.


1. Agree to become a monk
2. Ask about the school of thought
3. Attack the monks
4. Back to outside

endinTempleChoices

startschoolOfThought
Monk: "You seem to be a fighter. If you join us, we can teach you to be
undefeatable in battle... and the best part is that no one will die.
You see, stranger, our teachings can bypass death and mortal squabbles."

endschoolOfThought

startattack1
You swing at the burly man with your bare hands...
But it seems you've forgotten a key detail about your target.
He is burly! He easily blocks your punch and pummels you into the ground.


YOU DIED

endattack1

startattack2
You take aim and fire the gun at the burly man...
The burly man dies and you are arrested immediately.
The royal guards inform you that you will be excecuted tomorrow.

Hopefully you have an escape plan...

You got the "Guns Blazing" ending!
endattack2

startgiveDrink1
The burly man thanks you for your generosity and guzzles
the drink. Good thing your employer doesn't know that their
target is drinking on their dime.

endgiveDrink1

startgiveDrink2
You combine your 'Pint Of Intoxication' with your 'Succulent Of Necrosis'
to form a POISON. It looks no different than a normal drink.
The burly man thanks you for your generosity and guzzles
the drink. You swiftly exit the bar as the burly man begins to colapse.
By the time he dies you're long gone and claim your bounty.

You got the "Sneaky-Beaky-Like" ending!
endgiveDrink2

startdrunk
The world starts spinning and you realize you've had one too many.
With no inhibition, you climb onto a bar table and begin singing off
key at the top of your lungs. Your unsteady feet knock other patrons'
drinks off the table, shattering on the ground. The bartender drags you
by the collar to the curb outside where you promptly pass out.

Upon waking up, you suddenly remember why you were sent here and rush
back into the bar. But the burly man is nowhere to be found. Hungover and
frantic, you decide to flee rather than return to your employer with bad news.

You got the "Drunk and Disorderly" ending!

enddrunk

startrunaway
You become indoctrinated into the temple. You set out on a solo voyage to the
holy mountain. As you abandon your old life, you look back with a refreshed
outlook on life.

You got the "Runaway" ending!

endrunaway

startpalaceChoices
Two royal guards block the main gate.
There is a drifter idling by a tree.
You notice wanted posters everywhere.

1. Attack the guards
2. Attack the drifter
3. Talk to the drifter
4. Back to town center

endpalaceChoices

startskyrim
As you attempt your first blow on the drifer he snaps his fingers with a coy
smile. Magical sparks zoom from his fingers to your forehead. You fade into
blackness. The blackness fades and you wake up in the back of a horsedrawn
carraige, handcuffed. There are several others in handcuffs with you.
The one on your left says: "Hey, you, you're finally awake"

endskyrim

startdrifterChoices
The drifter gives you a coy smile...

1. Ask who he is
2. Give
3. Back to palace

enddrifterChoices

startpacifist
The drifter quickly chugs the drink. He smiles brightly and begins to cackle.
Rolling your eyes, you begin to walk away. The drifter taps your shoulder...

You turn around and gasp. The drifter is levitating off the ground and glowing.
The nearby guards don't seem to notice even though this is happening right in
front of them. Smiling even wider, the drifter floats to you...

Magic Drifter: "I know what you have to do... There is a better way!
I'll make that burly fellow look like him!"
The drifter gestures towards the wanted posters. You nod your head yes...


The smoke clears and you walk out of town towards your employers location...
As you walk you can see the burly man being led away by royal guards. Your
hands feel clean.

You got the "Pacifist / Drifter Friend" ending!

endpacifist

startattackMonksChoices
The monks gather in a defensive semi-circle at the first sign of your
aggression.

Head Monk: "Be warned stranger. Many have tried and none
have succeeded in defeating us in battle. I offer you one last chance to
reconsider."

1. Attack
2. Negotiate
3. Wait

endattackMonksChoices

startlimbo
You proceed with your attack despite the Head Monk's warning. As you lurch
into action, you notice your body begins to slow and freeze. The details of
the temple around you begin to fade into a dull whiteness. All sensations fade.
White nothingness surrounds you on all sides. You walk in the direction you
were facing but the landsacpe and horizon remain nothingness. You have a bad
feeling about this.

YOU DIED


...or did you?

endlimbo

startnegotiate
You explain that you've been sent to assassinate one of the townsfolk.
You beg the monks for an alternative to killing. The monks deliberate
and then inform you that they will indoctrinate the burly man into their
isolationist temple rather than allow him to die. Your hands feel clean.

You got the "Pacifist / Convert" ending!

endnegotiate

starthesitate
The monks neutralize you in your moment of hesitation.

You wake up on the outskirts of the town. You realize your employers
are looming over you. This can't be good...


"YOU GOT FIRED"

endhesitate
