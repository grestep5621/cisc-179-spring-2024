print("Tale of Snail\n---------------------")
print("Hello, you are a snail.\nThis is your tale.\nYou have one mission.\nTo reach the finish line before the hare. \nGood luck, don't disappoint.")


start = input("Do you accept your mission? ")
if start == "yes":
    print("let's begin")
    setting = ""
else:
    print("snail's are too cool for you anyways")
    quit()

shellcolor = input("Choose your snail shell: [b]lue or [p]ink")
name = input("enter your name ")
if shellcolor == "b":
    color = "blue"
elif shellcolor == "p":
    color = "pink"
else:
    color = "ugly"
print("Welcome to your life as a snail, " + name + ", I love your " + color + " shell, it really suits you :)")
print("The race will begin in 3...2...1!\n---------------------")
#first step
first_choice = input("You start your journey. Do you choose [w]inding road or [s]lippery slope?")
if first_choice == "w":
    print("Aww. The winding road takes longer for you, a snail, than slippery slopes\n---------------------")
elif first_choice == "s":
    print("Congrats! you are a snail so slippery slopes allow your slug self to go faster\n---------------------")
#second choice
second_choice = input("Do you choose to take a [b]reak and rest up, or [s]peed up?")
if second_choice == "s":
    print("You tire out and are forced to rest. Should have rested while you had the chance.\n---------------------")
    #third choice
    third_choice = input("A stranger approaches and offers a strange drink, [t]ake it or [n]o?")
    if third_choice == "t":
        print("It was coffee which made you crash from no sleep\n---------------------")
        message = input("You're behind. Write a message to encourage yourself to do better")
        fourth_choice = input("Do you take [a]viation alley, or [s]wamp way")
        if fourth_choice == "a":
            print("You are too behind. The Hare finishes before you even leave aviation alley.\nYou lose! :(")
            quit()
        elif fourth_choice == "s":
            print("The swamp enhances your slug abilities. You are seconds away from the finish line...\n---------------------")
            fifth_choice = input(
                "You have a choice. Do you attempt to [f]inish before the hare or [l]et him win because it will be good for his low self esteem")
            if fifth_choice == "f":
                print("The hare is faster and you look bad for being mean >:(")
                quit()
            elif fifth_choice == "l":
                print("Awww you're sweet. You win! :)")
                quit()
    elif third_choice == "n":
            print("Instead of drinking it you took a refreshing nap. \nYou're ahead! :)\n---------------------")
            fourth_choice = input("Do you take [a]viation alley, or [s]wamp way?")
            if fourth_choice == "a":
                print("You cannot fly so you trail behind :(\n---------------------")
                fifth_choice = input("You are seconds away from the finish line... You see a perched bird and right next to it you see the hare. Do you [t]rip the hare or [r]ide the bird")
                if fifth_choice == "t":
                    print("Cheater! The hare trips, and out of spite crushes you under his foot.\nYou lose! :(")
                    quit()
                if fifth_choice == "r":
                    print("The Bird Takes you to the finish line, milliseconds before the hare.\nYou win! :)")
                    quit()
            elif fourth_choice == "s":
                print("The swamp enhances your slug abilities.\nYou finish before the Hare and win the race! :)")
                quit()

elif second_choice == "b":
    print("Good! You got enough rest and have doubled your speed naturally.")
    #third choice
    third_choice = input("A stranger approaches and offers a strange drink, [t]ake it or [n]o?")
    if third_choice == "t":
        print("You get a caffeine rush and crash\n---------------------")
        message = input("You're behind. Write a message to encourage yourself to do better")
        fourth_choice = input("Do you take [a]viation alley, or [s]wamp way")
        if fourth_choice == "a":
            print("You are too behind. The Hare finishes before you even leave aviation alley.\nYou lose! :(")
            quit()
        if fourth_choice == "s":
            print("The swamp enhances your slug abilities. You are seconds away from the finish line...")
            fifth_choice = input("You have a choice. Do you attempt to [f]inish before the hare or [l]et him win because it will be good for his low self esteem?")
            if fifth_choice == "f":
                print("The hare is faster and you look bad for being mean.\nYou lose! :(")
                quit()
            elif fifth_choice == "l":
                print("Awww you're sweet.\nYou win! :)")
                quit()
    if third_choice == "n":
        print("You kept a steady course.\nYou're ahead! :)\n---------------------")
        fourth_choice = input("Do you take [a]viation alley, or [s]wamp way?")
        if fourth_choice == "a":
            print("You cannot fly so you trail behind :(\n---------------------")
            fifth_choice = input(
                "You are seconds away from the finish line... You see a perched bird and right next to it you see the hare. Do you [t]rip the hare or [r]ide the bird")
            if fifth_choice == "t":
                print("Cheater! The hare trips, and out of spite crushes you under his foot.\nYou lose! :(")
                quit()
            if fifth_choice == "r":
                print("The Bird Takes you to the finish line, milliseconds before the hare.\nYou win! :)")
                quit()
        elif fourth_choice == "s":
            print("The swamp enhances your slug abilities.\nYou finish before the Hare and win the race! :)")
            quit()





